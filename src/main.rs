extern crate clap;
extern crate hifitime;
extern crate nalgebra as na;
extern crate nyx_space as nyx;

use self::hifitime::datetime::*;
use self::hifitime::julian::*;
use clap::{App, AppSettings, Arg};
use na::{Vector6, U6};
use nyx::celestia::{State, EARTH, ECI};
use nyx::dynamics::celestial::TwoBody;
use nyx::dynamics::gravity::Harmonics;
use nyx::dynamics::Dynamics;
use nyx::io::gravity::MemoryBackend;
use nyx::propagators::{error_ctrl, PropOpts, Propagator, RK89};
use std::fs::File;
use std::io::Write;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;

#[derive(Clone)]
pub struct J2Dyn {
    pub twobody: TwoBody,
    pub harmonics: Harmonics<MemoryBackend>,
}

impl Dynamics for J2Dyn {
    type StateSize = U6;
    fn time(&self) -> f64 {
        // Both dynamical models have the same time because they share the propagator.
        self.twobody.time()
    }

    fn state(&self) -> Vector6<f64> {
        self.twobody.state() // Harmonics do not have a state of their own
    }

    fn set_state(&mut self, new_t: f64, new_state: &Vector6<f64>) {
        self.twobody.set_state(new_t, new_state);
    }

    fn eom(&self, _t: f64, state: &Vector6<f64>) -> Vector6<f64> {
        self.twobody.eom(_t, state) + self.harmonics.eom(_t, state)
    }
}

fn main() {
    let optargs = App::new("latlong")
        .version("0.1.0")
        .author("Christopher Rabotin <christopher.rabotin@gmail.com>")
        .about("An ultrafast orbit propagator to export latitude and longitude")
        .setting(AppSettings::AllowLeadingHyphen)
        .setting(AppSettings::ColoredHelp)
        .arg(
            Arg::with_name("position")
                .short("p")
                .long("position")
                .help("Initial position in km as \"X, Y, Z\"")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("velocity")
                .short("v")
                .long("velocity")
                .help("Initial velocity in km/s as \"VX, VY, VZ\"")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("time")
                .short("t")
                .long("time")
                .help("Propagation time, in seconds")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("mjd")
                .short("m")
                .long("mjd")
                .help("Starting Modified Julian date of the propagation")
                .takes_value(true)
                .default_value("51544.0"),
        )
        .arg(
            Arg::with_name("step")
                .short("s")
                .long("step")
                .help("Step size, in seconds")
                .takes_value(true)
                .default_value("5.0"),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .help("Output file")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    // Convert the position and velocity parameters;
    let position: Vec<f64> = optargs
        .value_of("position")
        .unwrap()
        .split(',')
        .map(|p| p.trim().parse().unwrap())
        .collect();

    let velocity: Vec<f64> = optargs
        .value_of("velocity")
        .unwrap()
        .split(',')
        .map(|p| p.trim().parse().unwrap())
        .collect();

    let prop_time: f64 = optargs.value_of("time").unwrap().parse().unwrap();

    let init = Vector6::from_row_slice(&[
        position[0],
        position[1],
        position[2],
        velocity[0],
        velocity[1],
        velocity[2],
    ]);

    let dt = ModifiedJulian {
        days: optargs.value_of("mjd").unwrap().parse().unwrap(),
    };

    let init_state = State::from_cartesian_vec::<EARTH, ModifiedJulian>(&init, dt, ECI {});
    println!("Starting state: \n{}", init_state);

    let filename = optargs.value_of("output").unwrap();
    println!("Writing output to: {}", filename);
    let mut file = File::create(filename).expect("could not create file");
    file.write_all(b"\"Modified Julian Date\",\"geodetic latitude\",\"geodetic longitude\"\n")
        .unwrap();

    let opts = PropOpts::with_fixed_step(
        optargs.value_of("step").unwrap().parse().unwrap(),
        error_ctrl::RSSStepPV {},
    );

    let mut j2dyn = J2Dyn {
        twobody: TwoBody::from_state_vec::<EARTH>(init),
        harmonics: Harmonics::from_stor::<EARTH>(MemoryBackend::j2_jgm3()),
    };

    // Set up the channels
    let (tx, rx): (Sender<(f64, Vector6<f64>)>, Receiver<(f64, Vector6<f64>)>) = mpsc::channel();

    thread::spawn(move || {
        let mut prop = Propagator::new::<RK89>(&mut j2dyn, &opts);
        prop.tx_chan = Some(&tx);
        prop.until_time_elapsed(prop_time);
    });

    loop {
        match rx.recv() {
            Ok((t, state_vec)) => {
                let this_dt = ModifiedJulian::from_instant(
                    dt.into_instant() + Instant::from_precise_seconds(t, Era::Present).duration(),
                );
                let rx_state =
                    State::from_cartesian_vec::<EARTH, ModifiedJulian>(&state_vec, this_dt, ECI {})
                        .in_ecef();
                file.write_fmt(format_args!(
                    "{:0.6},{:0.6},{:0.6},\n",
                    this_dt.days,
                    rx_state.geodetic_latitude(),
                    rx_state.geodetic_longitude()
                ))
                .unwrap();
            }
            Err(_) => {
                break;
            }
        }
    }
}
