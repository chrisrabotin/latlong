# latlong
A simple tools which propagates the initial state around the Earth applying only J2 (value from JGM3) for the requested time, and exports the Modified Julian Date, geodetic latitude and geodetic longitude into the requested output file.

# Prerequisites
0. Install git (needed for Rust)
1. Install Rust: https://www.rust-lang.org/learn/get-started
2. Download the code base from Gitlab
3. Using a console or command line, navigate to the folder where you have unzipped the code base

# Running
Display the help message (with the details of all parameters) with:
```
cargo run --release -- -h
```

Here is an example run:
```
cargo run --release -- -m 51544.0 -p "-2436.45,-2436.45,6891.037" -v "5.088611,-5.088611,0.0" -t 86400 -o test-output.csv
```
This will start the tool with the initial position of -2436.45, -2436.45, 6891.037 (X, Y, Z) km and 5.088611, -5.088611, 0.0 (VX, VY, VZ) km/s at the Modified Julian date time of 51544.0 (01 Jan 2000). Then, the tool will propagate for one full day (86 400 seconds) and output all of the latitude and longitudes every five seconds into the file `test-output.csv`. Such a run should last less than one second.

# Example output
Here is the first few lines in the output file from the example run above:
```
"Modified Julian Date","geodetic latitude","geodetic longitude"
51544.000058,63.559690,125.609601,
51544.000116,63.555952,126.186910,
51544.000174,63.549724,126.764010,
51544.000231,63.541007,127.340797,
51544.000289,63.529804,127.917168,
51544.000347,63.516117,128.493018,
51544.000405,63.499951,129.068246,
51544.000463,63.481310,129.642749,
51544.000521,63.460199,130.216426,
51544.000579,63.436625,130.789176,
51544.000637,63.410594,131.360902,
51544.000694,63.382112,131.931503,
51544.000752,63.351188,132.500885,
51544.000810,63.317831,133.068950,
51544.000868,63.282050,133.635605,
51544.000926,63.243853,134.200757,
```
